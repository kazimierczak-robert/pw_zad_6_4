__kernel void Power(__global int* a, __global int* b, __global int* c, int size)
{
    // Find position in global arrays.
    int n = get_global_id(0);
	int x = n/size;
	int y = n%size;
	int i = 0;
    // Bound check.
    if (n < (size*size))
    { 
		i=0;
		c[n]=0;
		for (i=0;i<size;i++)
		{
			c[n]+=a[(x*size)+(((x*size)+x+i)%size)]*b[((i+x)%size)*size+y];
		}
    }
}